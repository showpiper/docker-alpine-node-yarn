FROM mhart/alpine-node:6
MAINTAINER Eoin Shanaghy <eoin@showpiper.com>
RUN npm install -g yarn
